//
//  Readme.md
//  ProSpace
//
//  Created by PCCWS - User on 22/9/21.
//

// ini adalah test prospace. aplikasi ini memanfaatkan fitur UserDefaults dari native iOS.
//setiap properti dari mesin disimpan dalam userdefaults
//setiap gambar disimpan dalam format string menggunakan base64 encode. 
//setiap mesin disimpan menggunakan idnya sendiri. dan setiap property disimpan dengan tambahan suffix id dari mesin itu
//id mesin di generate alphanumeric 10 digit

// this is a prospace test. This app takes advantage of the UserDefaults feature of native iOS.
//every property of the machine is stored in userdefaults
// each image is saved in string format using base64 encode.
// each machine is stored using its own id. and each property is stored with the additional suffix id of that machine
// machine id generated 10 digit alphanumeric
